import Components as comp
import Constants as c
import pygame as pg
import random
import json


class State():
    def __init__(self,caption, estado_siguiente):
        self.caption = caption
        self.fin = False
        self.estado_siguiente = estado_siguiente
        self.ventana = pg.display.get_surface()

    def update(self):
        pass
    def setup(self):
        pg.display.set_caption(self.caption)
    def reboot(self):
        pass
    def boot(self):
        pass

    def quit(self):
        self.fin = True
        self.makequit()
    def makequit():
        pass


#-----------------Clase para las pantallas de carga e inicio--------------#
class PantallaInicio(State):
    def __init__(self, caption, estado_siguiente):
        State.__init__(self, caption, estado_siguiente)
        pass


class Level(State):
    def __init__(self, caption, estado_siguiente):
        State.__init__(self, caption, estado_siguiente)


    def update(self):

        self.ventana.fill(c.NEGRO)
        c.Grupos["player"].update()
        c.Grupos["player"].draw(self.ventana)

class Level1Tutorial(Level):
    def __init__(self, caption, estado_siguiente):
        Level.__init__(self, caption, estado__siguiente)

    def makeboot(self):
        pass
