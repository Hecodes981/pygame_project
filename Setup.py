import pygame as pg
import os
import Constants as c

def Setup():
    os.environ['SDL_VIDEO_CENTERED'] = '1';
    pg.init()
    pg.display.set_caption(c.CAPTION)
    ventana = pg.display.set_mode(c.window_size)
    ventana_rect = ventana.get_rect()