import pygame as pg
import Constants as c
import random

global_posicion_x = 0
global_posicion_y = 0

global_speed_x = 0
global_speed_y = 0

def recortarImagenes(img, size, scale = 1):
    img_rect = img.get_rect()[2:]
    matriz = []
    filas = img_rect[1]/size[1]
    columnas = img_rect[0]/size[0]

    for i in range(filas):
        matriz.append([])
        for j in range(columnas):
            frame = img.subsurface((j*size[0]), i*size[1], size[0], size[1])
            frame = pg.transform.scale(frame, (scale * size[0], scale * size[1]))
            matriz[i].append(frame)
    return matriz

class Player(pg.sprite.Sprite):
    def __init__(self):
        pg.sprite.Sprite.__init__(self)
        self.dicAnimacion = c.playerSheets
        self.image = self.dicAnimacion["Walk"][0][0]
        self.rect = self.image.get_rect()
        self.rect.x = 512
        self.rect.y = 512
        self.speedx = 0
        self.speedy = 0
        self.speed = 4

    def update():
        keystate = pg.key.get_pressed()
        if keystate[pg.K_LEFT]:
            self.speedx = self.speed

        self.rect.x += self.speedx
